# Health Guard - A Low-Touch Economy API Platform
![Health Guard](/splash.png)

## Vision
We want to create an API Platform useful to improve the Human Life Safety in a Low-Touch Economy scenario.

## Mission
Create an API Platform useful to fight COVID-19, providing features that can be implemented by third parties
such as Developer, IoT maker and Smart Devices manufacturer

## Team

- Alessandro Braccio
- Martino Lessio
- Massimo Canonico
- Andrea De Gaetano

## Description
![Health Guard](/splash2.png)


## Components
- proof-of-concept:  Component used as Debug Interface for the Mask Detector Visual Algorithm
- iot-application: IoT example application, which prove the use-case with a Raspberry 
- web-application: The Core application deployed on the Oracle Cloud Tier containing the APIs such as the Mask Detector Visual Algorithm and all datamodel used by the Artificial Intelligence and Machine Learning algorithms.
- images/: images used for testing the solution
- digital-assistan: Digital Assistant component code and configuration

## Features
Our Solution expose API useful to improve the Human Life Safety in a Low-Touch Economy scenario.

- Visual Analysis API: Useful to identify Mask Detection in real-time

- Thermal Analysis API: Useful to assess body temperature in real-time

- Voice Analysis API: Useful to assess voice issues in real-time

- Digital Assistant: Useful as interface between human and machine

### Mask Detector
Our Solution expose API useful to identify if a person wear a COVID-19 Mask Protection performing Image Analysis.

We have used Visual Artificial Intelligence algorithms and Machine Learning technique in order to create an algorithm able to:
- Identify and extract multiple person’s face on an acquired image or image flow.
- For every face identify if it wear a COVID-19 mask protection.

## Videos

### Smart Door + Digital Assistant Demo Proof of Concept
https://www.youtube.com/watch?v=LCVAdC5E8VA

### IoT Use Case
https://youtu.be/8KGy4vQcvcc

## Production Environment

### Endpoint Digital Assistant
http://132.145.17.130:3000/api/oracle/bot/chat?message=hi!

### Visual Analysis - Mask detector - API Gateway tester
http://132.145.17.130:5000/

### Smart Door + Digital Assistant Demo Proof of Concept
http://132.145.17.130:3000/

### Visual Analysis API - Mask Detector
(POST) https://nh5myxq74kdtroj7kpqxltajce.apigateway.uk-london-1.oci.customer-oci.com/api/check

#### Example Request
```
curl 'https://nh5myxq74kdtroj7kpqxltajce.apigateway.uk-london-1.oci.customer-oci.com/api/check' \
  -X 'POST' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 77735' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'DNT: 1' \
  -H 'Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryll1oqsb6drpIbCAR' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Accept-Language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7,la;q=0.6' \
  --compressed \
  --insecure
```
#### Example Response
  ```
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 51
Vary: Origin
Server: Werkzeug/1.0.1 Python/3.8.2
Date: Sat, 03 Oct 2020 16:15:03 GMT


  [{"status": true, "confidence": 89.37369585037231}]
  ```

![Health Guard](/logo.png)


