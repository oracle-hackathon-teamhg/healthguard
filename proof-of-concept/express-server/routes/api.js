var express = require('express');
var router = express.Router();
var request = require('request');
const OracleBot = require('@oracle/bots-node-sdk');

const {
    MessageModel
} = OracleBot.Lib;

const {
    WebhookClient,
    WebhookEvent
} = OracleBot.Middleware;

const CONSTANTS = require('../lib/constants');
const {
    text
} = require('body-parser');

const channel = {
    url: CONSTANTS.BOT_WEBHOOK_URL,
    secret: CONSTANTS.BOT_WEBHOOK_SECRET
};

const CONFIGURATION = {
    key: "TBD"
};

var activeSession = 1;

function getSession() {
    return CONSTANTS.CHATBOT_SESSION_ID + activeSession;
}

function newSession() {
    activeSession += 1;
}

const webhook = new WebhookClient({
    channel: channel
});

const logger = console;
webhook
    .on(WebhookEvent.ERROR, err => logger.error('Error:', err.message))
    .on(WebhookEvent.MESSAGE_SENT, message => logger.info('Message to bot:', message))
    .on(WebhookEvent.MESSAGE_RECEIVED, message => {
        // message was received from bot. forward to messaging client.
        logger.info('Message from bot:', message);
        logger.info()
        activeRequest.json({
            message: message.messagePayload.text
        });
    });
//

function sendEmail(recipient, text, callback) {

    console.log("Sorry, we are too late to write this function! ;) ");

    callback(null, {
        status: "OK"
    })
}





router.get('/oracle/configuration', async function (req, res, next) {

    res.json(CONFIGURATION);
});


// Sorry mom, but it's an hackathon! 
// OMG: THIS IS THE WORST THING A DEVELOPER CAN DO!!!!
// But, anyway... Please pretend you haven't seen this. 
// ¯\_(ツ)_/¯ 
var activeRequest = null;

// receive bot messages
router.post('/oracle/bot/message', webhook.receiver()); // receive bot messages

router.get('/oracle/bot/chat', (req, res) => {

    const MessageModel = webhook.MessageModel();

    var txt = '' + req.query.message;

    if (txt.toLocaleLowerCase().startsWith("hi")) {
        newSession();
    }

    const message = {
        userId: getSession(),
        messagePayload: MessageModel.textConversationMessage(txt)
    };

    webhook.send(message)
        .then((err, response) => {
            activeRequest = res;
            console.log("Message sent!");
        }, e => res.status(400).end());

});

router.post('/oracle/bot/chat', (req, res) => {

    const MessageModel = webhook.MessageModel();

    var txt = '' + req.body.message;

    if (txt.toLocaleLowerCase().startsWith("hi")) {
        newSession();
    }

    const message = {
        userId: getSession(),
        messagePayload: MessageModel.textConversationMessage(txt)
    };

    webhook.send(message)
        .then((err, response) => {
            activeRequest = res;
            console.log("Message sent!");
        }, e => res.status(400).end());

});


router.get('/oracle/triggerCheck', async function (req, res, next) {

    res.json({
        status: "TBD"
    });
});

router.get('/oracle/triggerCheckDelayed', async function (req, res, next) {

    res.json({
        status: "TBD"
    });
});



module.exports = router;