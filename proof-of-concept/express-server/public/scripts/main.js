$(document).ready(function () {
    window.openDoor = function openDoor() {
        $(".open-door").show();
        $(".locked-door").hide();
        $(".temp-high").hide();
        $(".no-mask").hide();

    }

    window.closeDoor = function closeDoor() {
        $(".open-door").hide();
        $(".temp-high").hide();
        $(".locked-door").show();
        $(".no-mask").hide();

    }

    closeDoor();

    function tempHigh() {
        $(".step0").hide();
        $(".step1").hide();
        $(".step2").hide();

        $(".temp-high").show();
        $(".locked-door").hide();
        $(".no-mask").hide();
    }

    function noMask() {
        $(".step0").hide();
        $(".step1").hide();
        $(".step2").hide();

        $(".temp-high").hide();
        $(".no-mask").show();
        $(".locked-door").hide();
    }


    function step0() {
        $(".step0").show();
        $(".step1").hide();
        $(".step2").hide();
    }

    function step1() {
        $(".step0").hide();
        $(".step1").show();
        $(".step2").hide();
    }

    function step2() {
        $(".step0").hide();
        $(".step1").hide();
        $(".step2").show();
    }

    step0();

    function sendMessage(txt) {
        $.post("http://132.145.17.130:3000/api/oracle/bot/chat", {
                message: txt
            })
            .done(function (data) {
                var msg = data.message;

                if(msg != "Hi there! What would you like me to echo back?"){
                    var html = `<p><strong>HG:</strong>${msg}</p>`;
                    $('.chat-body').append(html);    
                }

                if (txt == "hi!") {
                    step1();
                } else if (txt == "tempok") {
                    step2();
                } else if (txt == "tempko") {
                    tempHigh();
                }else if(txt.indexOf('passcode') !== -1){
                    setTimeout(function(){
                        var passcode = prompt("Please enter your passcode:");

                        sendMessage(passcode);
    
                    }, 1000)
                }else if(txt == '12345'){
                    openDoor();
                }

            });
    }


    $("#btnSubmit").click(function (event) {

        event.preventDefault();

        // Get form
        var form = $('#imgform')[0];

        // Create an FormData object
        var data = new FormData(form);

        // disabled the submit button
        $("#btnSubmit").prop("disabled", true);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://132.145.17.130:5000/",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);

                if(data instanceof Object && data.length > 0){
                    for(x of data){
                        if(x.status == false){
                            return noMask();
                        }
                    }
    
                    openDoor();    
                }
            },
            error: function (e) {

                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });

    });



    window.chatbot = {
        sendMessage: sendMessage
    }

});