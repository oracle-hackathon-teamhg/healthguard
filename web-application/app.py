import os
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from flask_cors import CORS, cross_origin
import numpy as np
import argparse
import cv2
import os
import json



app = Flask(__name__, static_url_path='', static_folder='static')
app.secret_key = os.urandom(24)
app.config['UPLOAD_FOLDER'] = '/tmp/'
app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg', 'gif'}
app.config['PROTOTXT'] = '/mnt/FileSystem-20200927-1236-33/detector/deploy.prototxt'
app.config['CAFFEMODEL'] = '/mnt/FileSystem-20200927-1236-33/detector/deploy.caffemodel'
app.config['MODEL'] = '/mnt/FileSystem-20200927-1236-33/detector/detector.model'
app.config['FACE_CONFIDENCE'] = 0.5

cors = CORS(app)

class Detection:
    def __init__(self,status=False,confidence=0):
        self.status = status
        self.confidence = confidence
    def setStatus(self, status):
        self.status = status
    def setConfidence(self, confidence):
        self.confidence = confidence

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS'] 


@app.route('/', methods=['GET'])
def form():
    return render_template('form.html')


@app.route('/', methods=['POST'])
@cross_origin()
def upload_file():
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # Load tensorflow face detector model
        net = cv2.dnn.readNet(app.config['PROTOTXT'], app.config['CAFFEMODEL'])
        model = load_model(app.config['MODEL'])
        # Load uploaded image and extract image spatial dimensions
        image = cv2.imread(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        orig = image.copy()
        (h, w) = image.shape[:2]
        # delete file from fs
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # Generate Image Blob
        blob = cv2.dnn.blobFromImage(image, 1.0, (300, 300),(104.0, 177.0, 123.0))
        # Run detection
        net.setInput(blob)
        detections = net.forward()
        # Output detections
        output_detections = []
        for i in range(0, detections.shape[2]):
            # extract confidence 
            confidence = detections[0, 0, i, 2]
            # filter out confidence
            if confidence > app.config['FACE_CONFIDENCE']:
                detection = Detection()
                # compute coordinates of face box
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                # ensure face box in the frame
                (startX, startY) = (max(0, startX), max(0, startY))
                (endX, endY) = (min(w - 1, endX), min(h - 1, endY))
                # extract the face square
                face = image[startY:endY, startX:endX]
                face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
                face = cv2.resize(face, (224, 224))
                face = img_to_array(face)
                face = preprocess_input(face)
                face = np.expand_dims(face, axis=0)
                # check face against mask detector
                (mask, withoutMask) = model.predict(face)[0]
                if mask > withoutMask:
                    detection.setStatus(True)
                else:
                    detection.setStatus(False)
                detection.setConfidence(max(mask, withoutMask) * 100)
                output_detections.append(detection)
        
        response = app.response_class(
                                        response=json.dumps([ob.__dict__ for ob in output_detections]),
                                        status=200,
                                        mimetype='application/json'
                                    )
        return response

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)