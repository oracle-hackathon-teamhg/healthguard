# Oracle-Hackathon Web Application - Face detection

## Build
```
docker build -t web-facedetection:latest .
```

## Run
```
docker run -d -p 5000:5000 web-facedetection
```

## Stop All
```
docker stop $(docker ps -a -q)
```

## Remove All Containers
```
docker rm $(docker ps -a -q)
```

## Remove All Docker Images
```
docker rmi $(docker images -q)
```

