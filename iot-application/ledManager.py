#!/usr/bin/python
#-*- coding: utf-8 -*-
import json
import codecs
import RPi.GPIO as GPIO # import a library

with codecs.open('output.json', 'r', 'utf8' ) as ff:
    data = json.loads(ff.read())

access_granted = False
GREENVALUE = GPIO.HIGH
REDVALUE = GPIO.LOW

print(data)

if len(data) > 0 and u"status" in data[0]:
  access_granted = data[0][u'status']

if access_granted == True:
  GREENVALUE = GPIO.LOW
  REDVALUE = GPIO.HIGH


GPIO.setmode(GPIO.BCM) # set the pin numbering system to BCM
GPIO.setup(17,GPIO.OUT) # set GPIO17 as an OUTPUT
GPIO.setup(27,GPIO.OUT) # set GPIO27 as am OUTPUT
#print "Access Granted?" +  str(access_granted) 

GPIO.output(17,REDVALUE) 
GPIO.output(27,GREENVALUE)
