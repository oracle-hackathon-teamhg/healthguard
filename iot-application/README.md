#Files
 - go.sh is the core of the application that talks with our solution running on Oracle Cloud
 - ledManager.py and ledOff.py are 2 scripts to control led on board

 The schema below is for a raspberry pi 3/4 but the pinout is compatible with pi zero used for the prototype.

 
 <img src="https://gitlab.com/ingenuity-oracle-hackathon/oracle-hackathon/-/raw/master/iot-application/pi3.png">
